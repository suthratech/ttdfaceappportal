import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { SuspectComponent } from './suspect/suspect.component';
import { AuthGuard } from './shared/auth.guard';
import { MissingComponent } from './missing/missing.component';
import { RegisterComponent } from './register/register.component';
import { UnknownComponent } from './unknown/unknown.component';


export const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path:"login", component:LoginComponent},
  { path:"dashboard", component:DashboardComponent,canActivate:[AuthGuard]},
  { path:"suspect", component:SuspectComponent,canActivate:[AuthGuard]},
  { path:"missing", component:MissingComponent,canActivate:[AuthGuard]},
  { path:"register", component:RegisterComponent,canActivate:[AuthGuard]},
  { path:"unknown", component:UnknownComponent,canActivate:[AuthGuard]}

];

export const navigatableComponents =[
  DashboardComponent,
  LoginComponent,
  SuspectComponent,
  MissingComponent,
  RegisterComponent,
  UnknownComponent

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

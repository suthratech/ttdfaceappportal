import { Component, OnInit } from '@angular/core';
import { FaceappserviceService } from '../shared/faceappservice.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SidenavService } from '../shared/sidenav.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  msg = '';



  constructor(private faceappservice: FaceappserviceService, private formBuilder: FormBuilder,
    public sidenavService: SidenavService, private router: Router) { }


  ngOnInit() {
    if (localStorage.getItem("faceapp_token")) {
     this.router.navigate(['/dashboard'])
    } 
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]

    });

  }

  get f() {
    return this.loginForm.controls;
  }

  onLogin(fd) {
    this.submitted = true;
    this.faceappservice.login(fd, 'users').subscribe(res => {
      console.log(res, 'res');
      let resp:any=res;
      localStorage.setItem("user_id", resp.id);
      console.log(localStorage.setItem("user_id", resp.id),'dffffffffffff');
      localStorage.setItem("faceapp_token", resp.token);
      this.faceappservice.emitLogin(true);
      this.sidenavService.emitShowSideNav(true);
      this.router.navigate(['/dashboard']);
    },
      (err) => {
        return;
      })

  }

}
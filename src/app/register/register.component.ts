import { Component, OnInit, OnDestroy } from '@angular/core';
import { FaceappserviceService } from '../shared/faceappservice.service';
import { timestamp } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit,OnDestroy {

  tableData: any = [];
  cols: any;
  regEvents: any;popup: boolean = false;
  enlargedImg:any;
  imgAngle=0;
  constructor(private faceapiservice: FaceappserviceService, private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'status', header: 'Case Type' },
      { field: 'PS_name', header: 'Police Station' },
      { field: 'fir_no', header: 'FIR Number' },
      { field: 'remarks', header: 'Remarks' },
      { field: 'timestamp', header: 'Date' },
      { field: 'image0', header: 'Image' },

    ]
    this.gettingRegistrationData();
    this.changeStreamForRegistration();
  }

  ngOnDestroy() {
    this.regEvents.close();
  }

  gettingRegistrationData() {
    this.faceapiservice.gettingFacesRegistrations('registrations').subscribe(res => {
      let data: any = res;
      this.tableData = data;
    })
  }

  changeStreamForRegistration() {
    let url = `${this.faceapiservice.baseUrl}registrations/change-stream`;
    this.regEvents = new EventSource(url);
    this.regEvents.addEventListener('data', msg => {
      this.gettingRegistrationData();
    })
  }
  imagePOPUP(data){
    this.imgAngle=0;
    this.enlargedImg = data;
    this.popup = true;
    setTimeout(() => {
      this.rotateImage(this.imgAngle);
    }, 50);
  }

  rotateImage(rang){
    this.imgAngle += rang;
    var img = document.getElementById('rimg');
       img?img.style.transform = `rotate(${this.imgAngle}deg)`:null;
  }



}

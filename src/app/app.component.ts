import { Component, ViewChild, HostListener, NgZone } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { SidenavService } from './shared/sidenav.service';
import { FaceappserviceService } from './shared/faceappservice.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('drawer', { static: true }) public sidenav: MatDrawer;
  showHeader: boolean = true;
  title = 'testportal';
  selectedItem: any;
  tabId:any;
  sidenavElements = [
    { id: 1, name: 'Dashboard', routerLink: "/dashboard", image: "./assets/home1.png" },
    { id: 2, name: 'Suspect', routerLink: "/suspect", image: "./assets/suspect.png" },
    { id: 3, name: 'Missing', routerLink: "/missing", image: "./assets/missing.png" },
    { id: 4, name: 'Unknown', routerLink: "/unknown", image: "./assets/suspect_sidenav.png" },
    { id: 5, name: 'Registered', routerLink: "/register", image: "./assets/registered.png" }
  ];

  constructor(public sidenavService: SidenavService, private router: Router, private faceappservice: FaceappserviceService) {
    this.faceappservice.loginStatus.subscribe(value => {
      this.showHeader = value;
    });
    this.sidenavService.showSideNav.subscribe(val => {
      this.showHeader = val;
      if (val) {
        this.sidenav.open();
      }
    });

  }

  ngOnInit(): void {
    if (localStorage.getItem("faceapp_token")) {
      this.sidenav.open();
      this.showHeader = true;
    } else {
      this.sidenav.close();
      this.showHeader = false;
    }

  }
  logOut() {
    localStorage.removeItem("faceapp_token");
    this.showHeader = false;
    this.selectedItem = "Dashboard";
    this.sidenav.close();
    localStorage.clear();
    this.router.navigate(["/login"]);
  }

}

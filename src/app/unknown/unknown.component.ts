import { Component, OnInit, OnDestroy } from '@angular/core';
import { FaceappserviceService } from '../shared/faceappservice.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unknown',
  templateUrl: './unknown.component.html',
  styleUrls: ['./unknown.component.css']
})
export class UnknownComponent implements OnInit, OnDestroy {

  tableData: any = [];
  cols: any;
  events: any;
  showRegForm: boolean = false;
  regForm: FormGroup;popup: boolean = false;
  case_types: any = [
    { name: 'Missing' },
    { name: 'Suspect' }
  ];
  selectedRecord: any;
  enlargedImg:any;
  imgAngle=0;
  constructor(private faceapiservice: FaceappserviceService,
    private fb: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.initiateRegForm();
    this.cols = [
      { field: '', header: 'Register' },
      { field: 'name', header: 'Name' },
      { field: 'age', header: 'Age' },
      { field: 'gender', header: 'Gender' },
      { field: 'beard', header: 'Beard' },
      { field: 'glasses', header: 'Sunglasses' },
      { field: 'cap', header: 'Cap' },
      { field: 'timestamp', header: 'Date' },
      { field: 'live_image', header: 'Image' }

    ];
    this.gettingFacesUnFoundCount();
    this.changeStream();
  }

  initiateRegForm() {
    this.regForm = this.fb.group({
      name: [''],
      fir_no: ['', Validators.required],
      status: ['', Validators.required],
      ps_name: ['', Validators.required],
      remarks: ['', Validators.required]
    });
  }

  gettingFacesUnFoundCount() {
    this.faceapiservice.gettingUnknownFaces('facedetections', 'false').subscribe(res => {
      let data: any = res;
      this.tableData = data;
    })
  }

  changeStream() {
    let url = `${this.faceapiservice.baseUrl}facedetections/change-stream`;
    this.events = new EventSource(url);
    this.events.addEventListener('data', msg => {
      this.gettingFacesUnFoundCount();
    });
  }

  ngOnDestroy() {
    this.events.close();
  }


  onRecordSelected(data) {
    this.selectedRecord = data;
    this.initiateRegForm();
    this.showRegForm = true;
  }
  imagePOPUP(data){
    this.imgAngle=0
    this.enlargedImg = data;
    this.popup = true;
    setTimeout(() => {
      this.rotateImage(this.imgAngle);
    }, 50);
  }
  onSubmit(fd) {
    !fd.name||fd.name==""?fd.name="Unknown":null;
    // const base64 = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIVFhUWFRUVFRUVFRUVFRUVFRUWFxUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0mHyUtLSstLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS03LS0tLf/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEDBAUGBwj/xAA9EAABAwIEAwYCCAUEAwEAAAABAAIRAyEEBRIxQVGBBhMiYXGRMqEHFBVCUrHB8GJygtHxI5Ky4UNTcxb/xAAaAQADAQEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAJhEAAgICAgIBBAMBAAAAAAAAAAECEQMxEiEEQRMUIjJRBXGBYf/aAAwDAQACEQMRAD8Adxss7Gq411lSxuy4D0UQYUoMeU2HKjxZR6NVsgCaE7AihRRdgwnCcBOAgY0JwnTFIGStamIRMRFUiSqVt5MsZ4Wzk6GTLRrFOAoMZim0m63m3zJ4ALGZmlarLqYa1gkSfFt6G5VQxSnowlkUdm+5wHEJ2uBXLYqvVHw031DeX6OP6Gyz6ucVWQNJbxFxPODMfqtn4rrpmP1CbPRKYsjhefu7aOZGpuqeIcOtv+1a/wD3TdIAadXmRHuvMz+DmcrSNVmj+zsyEKyaHaag4AyR5kLTp1Q4SDIOxC4p4Zw/JUaKSemOnISTErOihiExCeUimIEIghRIARKUoC5IORQEiQTSmBQBIkUwKRKVAOklKYlMBSlKjLk2tOgMlpsquL2UzHWUGL2XtiRUooMQiplDWQaoipooQsKPUkMGEQCEFFqSYxyhcEtST3JDJaRUrnBVmuTFydktBELWylYrXKyKztIaydTiB6D9/qrjFydIzyPjGyfMwKzwdfgadIHnxP5f9KxSkHQANDRG2rz4WJHHhK5/G5iQ6KRho8Ou5c4ixIJm0zdZ1PN6zQRUeS0kQRuJHGACDfeF6cIKKpHlybk7Z0+aZm9wLWVBcWhxEXj4tX5Bcbj9QdBl25nUSB+vlspsTmBe2NRcD98lzvF81mOdIgEk7lw3AHqroRI9jDe5I+L4ifmZ5Kq+oQRBMeUyeaelVaTsJ8+PCYKhq1HH4TbyBEDr+iANTJyC9zXB0HgTqPlax6LZwmZnCPmkZYT46fig+bQTZ22y5TDU3awBwIMx57+quZnXIMGRvH3gQfP24rPJjjOPGS6HFtO0etYfGCoxr2GWuEg+SPvCuU+jrNNbX0Hfdh7J5H4h7wf6iuvqMXzGfF8WRxPRhLkrGa5EomhTtWBQyZxRKNxQABlNdPKMKhAyU7UcJ2qRjpijTAIAYJFSKJxQBG5RyjqFRFWIzaWyixIspaeyDEbL1wRnMKaoUm7oXpmqI2o4QNUgQMGE8JkQCkoZM5FCEhAWO1M9JqZ6QgWKbN8cKLWinJeZHkBA1Hy3FvRRUmkzAveLxfhfnsrGeYKYECWtjVBkkgAui8CQNxwB4rt8aK2cPlS7UTkammSAY5yYvG0nhwvbZQUKRBhz9E76gSCD/LM8LhNjtQfcweMWv5/viiohwIBJ572473iV3I4yd1Sk1ugaTefiInkRJB6LPDrnQfy+UdVexFL4ZAIA97Dhy+YVeszS86QRaC38tggQzcK6zrRG1r8yDN0NJoJcDY6STxIjxWueQT13y0CLG8C46GFYwdENby1GAbxIiQT1I9kmNIDAUnOvTe3U3drtnCd28em/uhzWv3joLbxO0DgIneLcVfqYJoEk6d5BER5tItH97SsPS8nTJPLiEmBrdl8Z3OKpOmGzpd6OBaenHovXoXl1HJXjSSxwkbhttiZPK487eq9DyOqXYekS7UdAk84tfzXifymPuM/8Ovx5bRdLU4SlMCvJOkcoHNRpkARaUQCJPCAGTBGlCQCCMBM0IwkMEqF6sEKF7U4gVnFPCkFNH3SuxGHTFlHiNlO0WVfEiy9glaM4bpVEm7o6iZsiAI0AKLUgYKlaopUjXJDCQuCOUDigQITFMXJtSVDsv5E0GqGniR0gzPRdLmeWF4L2tEkSZBm1hJ/e65bJq2msw8zpP9Vv1XW5ha+pwIuNJIv+4XXjnxiceXHyyHC5p2bxBImm2DfbYzzG3I+qza2R1WRZzSN2ugO9AdnCPNeh4TNq19WkjeSIM8yRuoMf3lWwptd6kiPQraPkRMpeLNHA9z4dNRodG0TInawH7srX2fraJaTtBc4tdA58T/0t/AZBiata9IU2gRqdpcTG1wJPUldThexzQBNRxPMWb1aIlW88UTHx5PZ5v9jTHhETsDb1cfzK0WZFNOHXHC3lEr0NnZ2m0i3Xf2TY7AC0Bc2TPejqxYEtnlmY5YIi5MACCdxvIG8zEn3TZV2e1w5s8p2FtwZH9wuzx2WDcCD5E36KbK6Wg3Nh+z8lS8iyJ+LV0T/VO5w7S4avDfkOYPMKHIx/oM8Omxtv94wVfzfEB1MsNpiIBjhP6KKgwNAaOAhcH8jK4L+zPCqbDIQwjTQvIOoZNKcoAEgCTpgihMBkQQkJwkASIIQnSoYZQFqMJihAAAiTQnATA52VFXFlI945oKrrL2yEzKIumcUZ3QvTNkRgI4UbVKEDBIRtakWo2pANCYtREJNCAICEtKkcE0IQB4Si5zwGtLiLwN4HFdjmj9TA9pEQ2w5nees+yyeyLZ7+8eBt/KST+QQ1g5tXycA03trYSZ9n/JaR1Rm12mXqVIEC2+63cDQtsFy2KrkN0CQTxHAevBUjm1bCsDu972mTpE/i/CHRc72klCx8tFSycT0/C0rSLFG8lokweH+Vw+SdtO8Df9IweIBI6HiuhwGO7xrgQfYpOLjsFJS7NSnRk7iFHimArCxufOpkgNJMXuBf1K5HPO0eMLWv0lrC8M1AtiSCYJNgbFOONy0KWTidniqIkwQsvEMEwsLLsUTRp1SKxDyQ1ztxBIIc0GYkea28Kx7jqN/PmlKDiyoz5IfHPLcOSDGlzbm+72iL8bhWRcS0WEb7m26yMXVL6zcOPhBFR3q0HQ3qYPQLfcZgk/E0iNvhE/kCllqUaZEIJOTIEKNMvDENCUJVDAlc6ztDNfuxtMIUW9Eyko7OjhJIFJIseEoVXH49lFup5hcxW7btJhjZ81pDFOf4oiU4x2dg54G5hQnEjhdclVzMvgudAPBauHziixu4Wv08krI+ZM1X4l/BqWFdUJ8QgLOHaSh+Ie6t0M4ov2cs2mloaa/ZokhCagUQqNPFSBreazNDkcQJNiqVfMw3wkqfCGSZK5/O8Oe8svoFFNI5INrIzWw+JDrqZxWDhC5qsGu5S4nZGRptCkashtdykGIck0VZrApBZJxLkvrTkuI7NkpMKxTi3ofrj0cWLkbLwhKy2YtxN1afVslxZnLPGLpmz2bxOmqWHao0t6jxD/iR1WxnGF0upz+IGfWCT7CFwVPGvpva8btcHDoZXo+bt1sZUY4aYY9l7wYIj0v8ldFqSaKeIwzalhqO06WE9JMA9CrFbA0K1BtGoKjQxwc0tYwEOA3BNSyv4SoS1pc4utF4tzgKVjL2j9VUZ8QePkuzNwuX0qVNlNjKmlgIBLaImXEmTrk7+i18hwrqT3tdabtBINvXZBisIY1OPoCm7PsB18thFuET6qZS5DjBx6Ia2CmpUdBLSIsATJP8Thy5omYJtVpY4VC2RNN7abmyNiGmoo6jNFUz6tPpvJXQUsOHsDxvxhEZVoJQvZmU8taNIax0NEARTaAPIBysVsPpb8DhytPsQVpYaw49VmZ7Uhu5BNoDiBHG0xySbsVV0jnKTx34cdgYHTYz6rUzeqG0y8DYE9X2gdFnUDpe0wDfjtygjqh7W5i1nd0Ju4a3emw+c+ymuhZp8ItmP9uCbnZM7PmfiXN51hPvN4rHNIhYfTweznhlTR2mLz7wmHLm8urxV1nms9il70NuULDGNpezkz5Lmq9HfDP2AXKOhngcbLh6Rm62MqcJkrSPiYy5eRLRW7ZYp1UhuwWNhsPTG5gq52grg1LKgKZKtQpcY9IeNc3bLj8M07VPmqlbDEfelAcO7giFNypRa2zT4UyHuluZM6mwyVld07kkabk5Y1JU2XHCouztBnNPmjGc0/xLh9Dku6dyWP0kDU18Niw8SCpjS1b7rl8rc9rvILpcPX17brWUWtHFjy07Y4oQo6j2gwVdAWPiqDjUBiyltnTPMkui+KYRCmpWMRaEWdCZD3YS7sKbSnhBRD3QTd0FPpQPIF07Jk6VlWqBKhe7gEOJrclJhaUCSqfSo8yEXlnYYprr8hzAVMOKB+KlOnmWkkiPSY9lyjkeHqaXAjcEFCO/8Tu8Id4cR+XrBCuYVp31fJv9lk4KqCOH7urjMTAJnYbKXs6I6sfMa7nOaC46ZMjw7e260sie0aoIIngfJY/eBw/I+oEdU2En5xLTE+sfqqSvoUma2NLdclwEczCmy7EuAILzEwOREAjdUWU9bGwANzM+Ina87myFgDZEixTaoSaZt4vEQ2zj0MfkshwG5uTzuepKRxMiCbjjzVapWt6TxUjSKrvjERMiJMCfMgG3RZfbnIqgjEh+tjobOxY4CdB8tyD6qzWxTWVKZe6AXtaJ2m5/IfNehUsoa/CupPEd7LiPwkgaSOREA+qbx8of9OXyp98fR8+1674glS62mnJ3Wh2rySphqrmOG1weDgdnDyXL1ahEhYxjyPPvhKnom70Jq7tRACGjh5bKiIhapK+jF/c7Nqk8AADdINqmzbeizcupvqOho6rpqGDqtHNRknx6N4YnLswjTIdpduea3sLgLBVMdgKjvFGyPBZqW+Ai4WUpOUbib46g6ZpjLxyS+zhyR4THhxhaTWrD5JHZFpoyvs4ckvs4clr6EtKPkkUY/wBnDkn+zhyWtoS0p/JIR52/wmNpW3k9ANO8ysLM3AustbLscGtB4rv5aZ5lJNoLN6ppnVMeSgynMzUdpIS7QUHVWh44LPy0aHAjdNO49A001ejrbKRsALJp4l3FXW1pFlzyi2axyd1Ybgo3VOSgfrcVbYwBvmqbOmOZydURsJKr40GLK2oaqpF5VcGYtIwZKuMquOwUrgw8pUTrbK3s48dpdaDhydrSgbVPFWKZBQjdUzcyHFSAx0yNvMLWxEgwOXlf9/qudwFFzntbTBLyYaBuSulxuHLKxpkiW24XtuE6s3xyroxnayXA6ha0RE+cyrWXjEtkh+r+ElpHnI0j811eCyFphxnbY8Vfq5NTA8LCIMxMdFa0OUknRxRGNeCSQLiC4gRa40gSb+aqPdXYR4puJtAPOxXenCCR4dj94k3iLbj/AAn+xmVHCRPEShv9ApVs5vvXaJc0tMDh+5VU4gRJ4b/pC7HO8GwUTI+GduYmwK4rAZY/EVRSbIaINQjhOzZ5xPup49j+RVZf7G5QcVifrTxFKlLKTfxE/E48xw/wvSVXwOEbSptYwQAAAPIKywLQ8/JLk7Oc7YdnG4xgbIa8TpcRPq0xw29l4V2jyKrhappVmaTu07te38TXcR+zC+mKw29fyCyM/wAmo4lnd1mBzdxwc082u3aVk4K7RlNclR850rNUHdzuuz7W9iK2El9OatH8YHiZ/wDQD/kLei5HE0y0XBUw6uzHg0dN2fwzWsBAuVshc/lGKcGDwrVp4wcQuCd8merjaUUXgFh5pgdLtbR6rVZiWnipHgOEIhLi7HkipIgy5rHgOAur5EbLOy/Bmm43stQJSSvoIXXYqZlEQnASUGg2lLSiShAHmT6M1C1Q6ix2k81PoJquLdwVXzCSZO/FepH9Hlzj02joKT5on0XOMqaTJ5rXweNaKcHksl+HdUdDUodN2GSVxikWRjRa638E9umQVxrqJaYK0sHWMQqzK+0TjkoumjqKGIDgY3CJhWJhXlp4yreMxmlu91hSb6OmDr7n6NB7rKhVrjiVXwWLkGVj5k9/eeHibDcknYAcSt44r2TLyGy66iZmVpYciLrX7O9hsfiGg1KXctMeKqYcRzFMS73AXc5R9GWGpwaz31ncj4Gf7Rc9SrlEnHLi7PPcDllTEHTRpueeOkWHq7YdV1+T/RvU+LE1Q0fgp+J3VxsOgK9Ew+FZSaGU2tY0bBoAA6BERNlPEt5GzPyzJ6GFYTSpgcC43e7nLjw8tlzXa5kAYhvxM+IDdzOMDiReOey7HFutC5XtNTmkUvZtiXVmj2Xz7D4qkDRqB2kAOH3mn+Ju4Wu5u36fuy8RxmI+r1waJNN5phznMtqJe/cbGwatKh27x7N30njzpkOPrDoW3RShLaPW3NPDj8t04IYNRMAC5MCOZMryWt9IWOjwuogkzPduP5v2VCpmeMx7xSfUc/UbUxDWbXJA3AEm8o6DhP2dxmXaL6244bCDUDZ1c2YBsSwfe5T7Tuuh7P5Y2gwMY08y47lx3cTxJKrdnMhZh6YaLk3c7Yk/oOAC6OiyAlZlOS0gwE8piUJSMROd4uk+/wDhJ26jLrn98EYdKBAVKI4e3BcZ2j7C0qsvpAMfvp/8bvT8J9LeS7dJRKClsabWjxivlz6B0VGFh5EWPodiPRBoHEL2LF4JlRul7WubycJHTkuRzvsWIL8MYP8A63Gx/lcdj5H5Lkn47WjpjmT6ZxLsIwqJ2Cc34HKw5rmktcCCDBBsQfMImuXPRrSYGDrPmHDqtNqqNKma5SNKiwE6jBRAoKChKEwRJAY+b9gcTgqrnuipRO1RvD+ccPXZcbneHglfV1ai1wIcJB3BXnvbD6KqOK8eHqdw/iI1Uz/TNuhXtPFTtHnKa4cWfPDahW5l1bSJAuuuzX6GsbRaXUn06xH3RLHH01WJ6hcf9VrYZ/d16T6buT2lvsdiPRTOFnPFNO0R4imXk2uq2BfofDuCuV8UAZBWLiakklNK+i1S+47D6zT06rSsmpW1uWNTrO5q5g8cGmSEvj46Llkc2lo2KOD0DUvQ/olyCk4uxj2l1QVHU6ZI8LGhoJczm4kkTwiBuVweUOfja1PC0rOqOiSJDQAS5x8gASve+z+VMwtJmHpzppNIk7kkyXHzJcT1RFSWxzglLo0yfJRuRlRuKYEZKOQAhYOKhxNUXJNhc9EmUijmGLeHaadPXbU/xRDeEcyVi59mlClR7ys4sYeJBMkg+HTvqsbX2W60aWlx+J5k+Q4N6Cy8l+mKiR9WdqOlxqktnwh/g8QHCRPt6ohHlJGnJxicxmeYU6tfUwmCAG6rHw2CuNcHMB/fVcnqXpP0ddnhitVSqD3TSLAxrcRJbPCBE+oW+WCjo0w5ruyhkeQVsVUDKcx955+Fg3k+fkvWuzXZShgwS0lzju90SfIRYCVdwrRSaGUqTGNG0cP6QBfqmq4cOPj8Z/iggejdh0CwFPI5dejVaxSrBqPfQEsdLeNNx4cdDuHobei08vxzaokWPFps4dOXmmYtFpC5OShJQSRA3PqUcqKn+pScUAShyKVC1FqQBKHFEYKjaZRQgRgdp+zrcQNTYbVAs7g6Puu/vwXnNSk5ji1wIc0wQdwQvZRdcp24yQOZ3zI1sHiA3czzHMb+krlzYr+5G+LJTpnEMUzVVY9TseuNo6iw0IwFE1ykaUUBIAiAQgopRxA9fY4qZoSSXvnlIeFn51k9DFUzTr0mvaeBFx5g7g+YTpJMZ8+fSF9HFfBaq1GauGmSd30h/GBu3+IdV585JJQ1RDJQwkQEbWhvmef9kklviiuNgj036EcBNariyLMDaLf5qh1PPQNZ/uK9mY4az6CUklzz2ax0EXKIpJLMYFeoGjeJVC7jezQZA4k8CeUckySlmkV0DinTZcX9K+TGtg2vaJdRcXxzbHiHtJ6JJJwdMJHiLWkgkCwX0V9HuFDMuwoAiaTXnzL/ABEn3SSXTn0jOGzo2tScYHmkkuU1Kb6JeboxhYgjcbEWI9CkkqQmyZmNc2zwXD8Q+Lq3j09lao4hrgSHAxvzHqOCSSGJoaibIoHFMkgQ4HFASkkgRIxTn5Rv+qSSYEdGXXO3IWn1UZeKgcIEaoFuDeXWUkkgPE+1FN+FxVSlYNnUySB4HXG/K46LObmT+bf9zf7p0ltHxoOKZSzyRMzMqnL2c3+6lGaVPwH2lJJTLxoFrO36JG504btI9QjGfpJLB4onRGVn/9k=';
    const imageName = fd.fir_no + '.jpg';
    const imageBlob = this.dataURItoBlob(this.selectedRecord.live_image);
    const imageFile = new File([imageBlob], imageName, { type: 'image/jpg' });
    const formData = new FormData();
    formData.append('image0', imageFile);
    formData.append('key', 'police');
    formData.append('name', fd.name);
    formData.append('fir_no', fd.fir_no);
    formData.append('status', fd.status.name);
    formData.append('ps_name', fd.ps_name);
    formData.append('remarks', fd.remarks);
    this.faceapiservice.registerRequestToAI(formData).subscribe(res => {
      console.log(res);
      let resp: any = res;
      if (resp.msg == "Registration completed. Thanks.") {
        this.saveToDB(resp);
      } else {
        alert('This person is already existed or No faces are detected in this image');
      }
    }, (err) => {
      console.log(err);
      alert('Error Occured, Please Try again');
    });
  }

  saveToDB(data) {
    this.faceapiservice.saveRegisteredRecord(data).subscribe(res => {
      this.router.navigate(["/register"]);
    }, (err) => {
      console.log(err);
    })
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpg' });
    return blob;
  }


  onCancel() {
    this.initiateRegForm();
    this.showRegForm = false;
    this.selectedRecord = null;
  }
  rotateImage(rang){
    this.imgAngle += rang;
    var img = document.getElementById('rimg');
       img?img.style.transform = `rotate(${this.imgAngle}deg)`:null;
  }

}

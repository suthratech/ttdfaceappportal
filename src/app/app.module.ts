import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes, navigatableComponents } from "./app-routing.module";
import { RouterModule } from '@angular/router';
import { MatModule } from './mat.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material';
import { TableModule } from 'primeng/table';
import { CdkTableModule } from '@angular/cdk/table';
import { MatTableModule } from '@angular/material';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { SuspectComponent } from './suspect/suspect.component';
import { DialogModule } from 'primeng/dialog';
import { MissingComponent } from './missing/missing.component';
import {InputTextModule} from 'primeng/inputtext';
import { SanitizerPipe } from './shared/sanitizer.pipe';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import { RegisterComponent } from './register/register.component';
import { UnknownComponent } from './unknown/unknown.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    SuspectComponent,
    MissingComponent,
    SanitizerPipe,
    RegisterComponent,
    UnknownComponent
  ],
  imports: [
    CalendarModule,
    MatModule,
    OverlayPanelModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    InputTextModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    TableModule,
    CdkTableModule,
    MatTableModule,
    DropdownModule,
    DialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FaceappserviceService } from '../shared/faceappservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  dateTime: any;
  faceFoundCount: number;
  faceunFoundCount: number;
  totalfacesRegistrationCount: number;
  events: any;
  regEvents: any;
  totalFacesSuspectedCount: number;
  totalFacesMissingCount: number;
  fDate: Date;
  tDate: Date;

  constructor(private faceappserivce: FaceappserviceService,private router: Router) { }

  ngOnInit() {
    this.gettingFacesFoundCount();
    this.gettingFacesUnFoundCount();
    this.gettingFacesRegistrationCount();
    this.gettingSuspectCount();
    this.changeStream();
    this.changeStreamForRegistration();
    this.gettingSuspectCount();
    this.gettingMissingCount();
  }

  ngOnDestroy() {
    this.events.close();
    this.regEvents.close();
  }

  gettingFacesFoundCount() {
    this.faceappserivce.gettingFacesCount('facedetections', 'true').subscribe(res => {
      let data: any = res;
      this.faceFoundCount = data.count;
    })
  }
  gettingFacesUnFoundCount() {
    this.faceappserivce.gettingFacesCount('facedetections', 'false').subscribe(res => {
      let data: any = res;
      this.faceunFoundCount = data.count;
    })
  }
  changeStream() {
    let url = `${this.faceappserivce.baseUrl}facedetections/change-stream`;
    this.events = new EventSource(url);
    this.events.addEventListener('data', msg => {
      this.gettingFacesFoundCount();
      this.gettingFacesUnFoundCount();
      this.gettingSuspectCount();
      this.gettingMissingCount();
      this.gettingFacesRegistrationCount();
      this.fDate = null;
      this.tDate = null;
    })
  }
  changeStreamForRegistration() {
    let url = `${this.faceappserivce.baseUrl}registrations/change-stream`;
    this.regEvents = new EventSource(url);
    this.regEvents.addEventListener('data', msg => {
      this.gettingFacesRegistrationCount();
      this.gettingFacesFoundCount();
      this.gettingFacesUnFoundCount();
      this.gettingSuspectCount();
      this.gettingMissingCount();
      this.fDate = null;
      this.tDate = null;
    })
  }
  gettingFacesRegistrationCount() {
    this.faceappserivce.gettingFacesRegistrationCount('registrations').subscribe(res => {
      let data: any = res;
      this.totalfacesRegistrationCount = data.count;
    })
  }

  gettingSuspectCount() {
    this.faceappserivce.gettingSuspectDataCount('facedetections', 'Suspect').subscribe(res => {
      let data: any = res;
      this.totalFacesSuspectedCount = data.count;
    })
  }
  gettingMissingCount() {
    this.faceappserivce.gettingSuspectDataCount('facedetections', 'Missing').subscribe(res => {
      let data: any = res;
      this.totalFacesMissingCount = data.count;
    })
  }
  dateWiseFilter() {
    if(!this.tDate && !this.tDate){
      return;
    }
    this.faceappserivce.gettingFilterData(this.fDate, this.tDate).subscribe(res => {
      let data: any = res;
      this.faceFoundCount = data.known;
      this.faceunFoundCount = data.unknown;
      this.totalFacesSuspectedCount = data.suspect;
      this.totalFacesMissingCount = data.missing;
      this.totalfacesRegistrationCount = data.registered;
    })
  }

  routeToPage(link){
    this.router.navigate(["/"+link]);
  }

}

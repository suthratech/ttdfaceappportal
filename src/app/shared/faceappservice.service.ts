import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class FaceappserviceService {
  baseUrl = environment.baseUrl;
  isLogin$: Subject<any> = new BehaviorSubject<any>(null);
  postHeaders() {
    let headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    return headers;
  }

  get loginStatus(): BehaviorSubject<any> {
    return this.isLogin$ as BehaviorSubject<any>;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error Occured : ', error.error.message);
    } else {
      console.error('Backend ERROR : ', error.error);
    }

    return throwError(error);
  }

  constructor(private http: HttpClient) { }

  login(fd, model) {
    let url = `${this.baseUrl}${model}/login`;
    let headers = this.postHeaders();
    return this.http.post(url, fd, { headers: headers })
      .pipe(
        catchError(this.handleError)
      )
  }

  gettingSuspectData(model, status,facefound) {
    let url = `${this.baseUrl}${model}?filter[where][status]=${status}&filter[where][face_found]=${facefound}&filter[order]=timestamp DESC`;
    console.log(url);
    return this.http.get(url)
  }

  emitLogin(value: any) {
    this.isLogin$.next(value);
  }
  gettingFacesCount(model, status) {
    let url = `${this.baseUrl}${model}/count?[where][face_found]=${status}`;
    return this.http.get(url)
  }
  gettingFacesRegistrationCount(model) {
    let url = `${this.baseUrl}${model}/count`;
    return this.http.get(url)
  }
  gettingFilterData(fdate, tdate) {
    let data = {
      fDate: fdate,
      tDate: tdate
    }
    let url = `${this.baseUrl}facedetections/filterData`;
    let headers = this.postHeaders();
    return this.http.post(url, data, { headers: headers }).pipe(
      catchError(this.handleError)
    )

  }
  gettingSuspectDataCount(model, status) {
    let url = `${this.baseUrl}${model}/count?[where][status]=${status}`;
    return this.http.get(url)

  }
  gettingUnknownFaces(model,status){
    let url = `${this.baseUrl}${model}?filter[where][face_found]=${status}&filter[order]=timestamp DESC`;
    console.log(url,'url');
    return this.http.get(url)
  }
  gettingFacesRegistrations(model){
    let url = `${this.baseUrl}${model}?filter[order]=timestamp DESC`;
    console.log(url,'url');
    return this.http.get(url)
  }

  registerRequestToAI(data){
    let url = `http://216.229.79.86:9999/register`;
    return this.http.post(url,data).pipe(catchError(
      this.handleError
    ));
  }

saveRegisteredRecord(data){
  let url = `${this.baseUrl}registrations`;
  let headers = this.postHeaders();
  return this.http.post(url,data,{headers:headers}).pipe(catchError(
    this.handleError
  ));
}

}

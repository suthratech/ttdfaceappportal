import { Component, OnInit , OnDestroy } from '@angular/core';
import { FaceappserviceService } from '../shared/faceappservice.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-suspect',
  templateUrl: './suspect.component.html',
  styleUrls: ['./suspect.component.css']
})
export class SuspectComponent implements OnInit,OnDestroy {
  tableData: any = [];
  cols: any;
  display: boolean = false;
  images: any;
events:any;popup: boolean = false;
overlayImg:string;
enlargedImg:any;
imgAngle=0;
  constructor(private faceappservice: FaceappserviceService, private domSanitizer: DomSanitizer) {
    this.gettingSusectData();

  }
  ngOnDestroy() {
    this.events.close();
  }

  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'age', header: 'Age' },
      { field: 'gender', header: 'Gender' },
      { field: 'status', header: 'Case Type' },
      { field: 'policestation_name', header: 'Police Station' },
      { field: 'fir_number', header: 'FIR Number' },
      { field: 'beard', header: 'Beard' },
      { field: 'glasses', header: 'Glasses' },
      { field: 'cap', header: 'Cap' },
      { field: 'timestamp', header: 'Date' },
      { field: 'image', header: 'Image' },
      { field: 'live_image', header: 'Live Image' },
    ];

    this.changeStream();
  }

  gettingSusectData() {
    this.faceappservice.gettingSuspectData('facedetections', 'Suspect', 'true').subscribe(res => {
      this.tableData = res;
    });


  }

  changeStream() {
    console.log('Change Stream')
    let url = `${this.faceappservice.baseUrl}facedetections/change-stream`;
    this.events = new EventSource(url);
    this.events.addEventListener('data', msg => {
      this.gettingSusectData();
    });

    this.events.onerror = function() {
      console.log("EventSource failed.");
    };

  }
  imagePOPUP(data){
    this.imgAngle=0;
    this.enlargedImg = data;
    this.popup = true;
    setTimeout(() => {
      this.rotateImage(this.imgAngle);
    }, 50);
  }

  rotateImage(rang){
    this.imgAngle += rang;
    var img = document.getElementById('rimg');
       img?img.style.transform = `rotate(${this.imgAngle}deg)`:null;
  }
  
}

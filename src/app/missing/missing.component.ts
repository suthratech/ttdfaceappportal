import { Component, OnInit ,OnDestroy } from '@angular/core';
import { FaceappserviceService } from '../shared/faceappservice.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-missing',
  templateUrl: './missing.component.html',
  styleUrls: ['./missing.component.css']
})
export class MissingComponent implements OnInit ,OnDestroy {

  tableData: any = [];
  cols: any;
  display: boolean = false;
  images: any;
  base64Image: any;
  events: any;
  overlayImg: string;popup: boolean = false;
  enlargedImg:any;
  imgAngle=0;
  constructor(private faceappservice: FaceappserviceService, private domSanitizer: DomSanitizer) { }


  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'age', header: 'Age' },
      { field: 'gender', header: 'Gender' },
      { field: 'status', header: 'Case Type' },
      { field: 'policestation_name', header: 'Police Station' },
      { field: 'fir_number', header: 'FIR Number' },
      { field: 'beard', header: 'Beard' },
      { field: 'glasses', header: 'Glasses' },
      { field: 'cap', header: 'Cap' },
      { field: 'timestamp', header: 'Date' },
      { field: 'image', header: 'Image' },
      { field: 'live_image', header: 'Live Image' }
    ];
    this.gettingSusectData();
    this.changeStream();
  }

  ngOnDestroy() {
    this.events.close();
  }

  gettingSusectData() {
    this.faceappservice.gettingSuspectData('facedetections', 'Missing','true').subscribe(res => {
      this.tableData = res;
    })
  }

  changeStream() {
    let url = `${this.faceappservice.baseUrl}facedetections/change-stream`;
    this.events = new EventSource(url);
    this.events.addEventListener('data', msg => {
      this.gettingSusectData();
    });
  }
  View(image) {
    this.display = true;
    this.images = this.domSanitizer.bypassSecurityTrustResourceUrl(image);
  }
  imagePOPUP(data){
    this.imgAngle=0;
    this.enlargedImg = data;
    this.popup = true;
    setTimeout(() => {
      this.rotateImage(this.imgAngle);
    }, 50);
  }

  rotateImage(rang){
    this.imgAngle += rang;
    var img = document.getElementById('rimg');
       img?img.style.transform = `rotate(${this.imgAngle}deg)`:null;
  }
}
